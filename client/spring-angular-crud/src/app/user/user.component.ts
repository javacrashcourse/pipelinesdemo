import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: []
})
export class UserComponent implements OnInit {

  public users: User[];

  constructor(private router: Router, private userService: UserService) {

  }

  public ngOnInit() {
    this.userService.getUsers()
      .subscribe(data => {
        console.log(data);
        this.users = data;
      });
  }

  public deleteUser(id: number): void {
    this.userService.deleteUser(id)
      .subscribe( data => {
        this.users = this.users.filter(u => u.id !== id);
      })
  }
}
